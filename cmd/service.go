package cmd

import (
	"axiata-test/domains"
	"axiata-test/domains/data/repositories"
	"axiata-test/domains/middlewares"

	"go.uber.org/zap"
)

func InitService(logger *zap.Logger, repo repositories.Repositories) domains.Service {
	serviceBuilder := domains.NewServiceBuilder(logger, repo)
	serviceBuilder.SetMiddlewares(getServiceMiddleware(logger))
	return serviceBuilder.Build()
}

func getServiceMiddleware(logger *zap.Logger) []domains.Middleware {
	var mw []domains.Middleware
	var repo repositories.Repositories
	mw = addDefaultServiceMiddleware(logger, mw, repo)
	return mw
}

func addDefaultServiceMiddleware(logger *zap.Logger, mw []domains.Middleware, repo repositories.Repositories) []domains.Middleware {
	return append(mw, middlewares.LoggingMiddleware(logger, repo))
}
