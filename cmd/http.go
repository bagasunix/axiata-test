package cmd

import (
	"net/http"

	"axiata-test/endpoints"
	transportHttp "axiata-test/transports/http"

	"go.uber.org/zap"
)

func InitHttpHandler(logger *zap.Logger, endpoints endpoints.Endpoints) http.Handler {
	return transportHttp.NewHttpHandler(logger, endpoints)
}
