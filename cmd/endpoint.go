package cmd

import (
	"axiata-test/domains"
	"axiata-test/endpoints"
	"axiata-test/endpoints/middlewares"

	"go.uber.org/zap"
)

func InitEndpoint(logger *zap.Logger, s domains.Service) endpoints.Endpoints {
	a := endpoints.NewBuilder()
	a.SetMiddlewares(getEndpointMiddleware(logger))
	a.SetService(s)
	return a.Build()
}

func getEndpointMiddleware(logger *zap.Logger) (mw map[string][]endpoints.Middleware) {
	mw = map[string][]endpoints.Middleware{}
	addDefaultEndpointMiddleware(logger, mw)
	return mw
}

func middlewaresWithAuthentication(logger *zap.Logger, method string) []endpoints.Middleware {
	mw := defaultMiddlewares(logger, method)
	return mw
}

func defaultMiddlewares(logger *zap.Logger, method string) []endpoints.Middleware {
	return []endpoints.Middleware{
		middlewares.Logging(logger.With(zap.String("method", method))),
	}
}

func addDefaultEndpointMiddleware(logger *zap.Logger, mw map[string][]endpoints.Middleware) {
}
