package cmd

import (
	"axiata-test/domains/data/repositories"
	"axiata-test/pkg/config"
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"go.uber.org/zap"
)

func Run() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	logger := InitLogger()
	config.InitConfig(ctx, logger)
	db := InitDb(ctx, logger, config.Config)
	repositories := repositories.New(logger, db)
	svc := InitService(logger, repositories)
	eps := InitEndpoint(logger, svc)

	httpHandler := InitHttpHandler(logger, eps)
	errs := make(chan error)
	defer close(errs)
	go initCancel(errs)
	go initHttp(httpHandler, errs)
	logger.Error("exit", zap.Error(<-errs))
}

func initCancel(errs chan error) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	errs <- fmt.Errorf("%s", <-c)
}

func initHttp(httpHandler http.Handler, errs chan error) {
	server := &http.Server{
		Addr:    ":" + strconv.Itoa(config.Config.Server.Port),
		Handler: httpHandler,
	}
	errs <- server.ListenAndServe()
}
