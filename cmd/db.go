package cmd

import (
	"axiata-test/pkg/config"
	"axiata-test/pkg/db"
	"axiata-test/pkg/errors"
	"context"
	"database/sql"
	"time"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

func InitDb(ctx context.Context, logger *zap.Logger, configs *config.Cfg) *sql.DB {
	configBuilder := &db.DbConfig{
		Driver:       configs.Database.Driver,
		Host:         configs.Database.Host,
		Port:         configs.Database.Port,
		User:         configs.Database.User,
		Password:     configs.Database.Password,
		DatabaseName: configs.Database.DbName,
	}

	return NewDB(ctx, logger, configBuilder)
}

func NewDB(ctx context.Context, logger *zap.Logger, dbConfig *db.DbConfig) *sql.DB {
	// Membuat koneksi ke database dengan DSN dari dbConfig
	db, err := sql.Open(dbConfig.Driver, dbConfig.GetDSN())
	if err != nil {
		errors.HandlerWithOSExit(logger, err, "init", "database", "config", dbConfig.GetDSN())
	} // Inisialisasi driver migrasi PostgreSQL
	// driver, err := postgres.WithInstance(db, &postgres.Config{})
	// if err != nil {
	// 	errors.HandlerWithOSExit(logger, err, "failed to initialize postgres driver")
	// }

	// // Buat instance migrasi
	// m, err := migrate.NewWithDatabaseInstance(
	// 	"file://../migrations",
	// 	"postgres", driver)
	// if err != nil {
	// 	errors.HandlerWithOSExit(logger, err, "failed to create migration instance")
	// }

	// // Jalankan migrasi ke versi terbaru
	// if err := m.Up(); err != nil && err != migrate.ErrNoChange {
	// 	errors.HandlerWithOSExit(logger, err, "failed to apply migrations")
	// }

	// fmt.Println("Migration successful")

	// Mengatur parameter koneksi
	db.SetMaxIdleConns(dbConfig.MaxIdleConns)
	db.SetMaxOpenConns(dbConfig.MaxOpenConns)
	db.SetConnMaxLifetime(time.Hour)

	// Verifikasi koneksi
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := db.PingContext(ctx); err != nil {
		errors.HandlerWithOSExit(logger, err, "init", "database", "ping", "")
	}

	return db
}
