package endpoints

import (
	"axiata-test/domains"
	"axiata-test/endpoints/requests"
	"context"
)

const (
	CREATE_POST = "CreatePost"
	LIST_POST   = "ListPost"
	DELETE_POST = "DeletePost"
	VIEW_POST   = "ViewPost"
	UPDATE_POST = "UpdatePost"
)

type PostEndpoint struct {
	CreatePostEndpoint Endpoint
	ListPostEndpoint   Endpoint
	DeletePostEndpoint Endpoint
	ViewPostEndpoint   Endpoint
	UpdatePostEndpoint Endpoint
}

func NewPostEndpoint(s domains.Service, mdw map[string][]Middleware) PostEndpoint {
	eps := PostEndpoint{}
	eps.CreatePostEndpoint = makeCreatePostEndpoint(s)
	eps.ListPostEndpoint = makeListPostEndpoint(s)
	eps.DeletePostEndpoint = makeDeletePostEndpoint(s)
	eps.ViewPostEndpoint = makeViewPostEndpoint(s)
	eps.UpdatePostEndpoint = makeUpdatePostEndpoint(s)

	SetEndpoint(CREATE_POST, &eps.CreatePostEndpoint, mdw)
	SetEndpoint(LIST_POST, &eps.ListPostEndpoint, mdw)
	SetEndpoint(DELETE_POST, &eps.DeletePostEndpoint, mdw)
	SetEndpoint(VIEW_POST, &eps.ViewPostEndpoint, mdw)
	SetEndpoint(UPDATE_POST, &eps.UpdatePostEndpoint, mdw)

	return eps
}

func makeCreatePostEndpoint(s domains.Service) Endpoint {
	return func(ctx context.Context, request any) (response any, err error) {
		return s.CreatePost(ctx, request.(*requests.CreatePost))
	}
}

func makeListPostEndpoint(s domains.Service) Endpoint {
	return func(ctx context.Context, request any) (response any, err error) {
		return s.ListPost(ctx)
	}
}

func makeDeletePostEndpoint(s domains.Service) Endpoint {
	return func(ctx context.Context, request any) (response any, err error) {
		return s.DeletePost(ctx, request.(*requests.EntityId))
	}
}
func makeViewPostEndpoint(s domains.Service) Endpoint {
	return func(ctx context.Context, request any) (response any, err error) {
		return s.ViewPost(ctx, request.(*requests.EntityId))
	}
}
func makeUpdatePostEndpoint(s domains.Service) Endpoint {
	return func(ctx context.Context, request any) (response any, err error) {
		return s.UpdatePost(ctx, request.(*requests.UpdatePost))
	}
}
