package requests

import (
	"axiata-test/pkg/errors"
	"encoding/json"
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gofrs/uuid"
)

type CreatePost struct {
	Title       string    `json:"title"`
	Content     string    `json:"content"`
	Tags        []string  `json:"tags"`
	Status      string    `json:"status"`
	PublishDate time.Time `json:"publish_date"`
}

func (s *CreatePost) ToJSON() []byte {
	j, err := json.Marshal(s)
	errors.HandlerReturnedVoid(err)
	return j
}

func (c CreatePost) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Title, validation.Required.Error("Judul harus diisi")),
		validation.Field(&c.Status, validation.Required.Error("Status harus diisi")),
	)
}

type UpdatePost struct {
	ID          uuid.UUID `json:"id"`
	Title       string    `json:"title"`
	Content     string    `json:"content"`
	Tags        []string  `json:"tags"`
	Status      string    `json:"status"`
	PublishDate time.Time `json:"publish_date"`
}

func (s *UpdatePost) ToJSON() []byte {
	j, err := json.Marshal(s)
	errors.HandlerReturnedVoid(err)
	return j
}

func (c UpdatePost) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ID, validation.Required.Error("ID tidak valid")),
	)
}
