package responses

import (
	"axiata-test/pkg/errors"
	"encoding/json"
)

type BaseResponse struct {
	ResponseData   any    `json:"response_data"`
	ResponseDesc   string `json:"response_description"`
	HTTPStatusCode int    `json:"http_status_code"`
}

func (c *BaseResponse) ToJSON() []byte {
	j, err := json.Marshal(c)
	errors.HandlerReturnedVoid(err)
	return j
}
