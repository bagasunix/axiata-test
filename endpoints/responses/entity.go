package responses

import (
	"axiata-test/pkg/errors"
	"encoding/json"
)

type EntityId struct {
	Id any `json:"id"`
}

func (c *EntityId) ToJSON() []byte {
	j, err := json.Marshal(c)
	errors.HandlerReturnedVoid(err)
	return j
}
