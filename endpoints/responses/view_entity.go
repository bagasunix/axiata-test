package responses

import (
	"axiata-test/pkg/errors"
	"encoding/json"
)

type ViewEntity[T any] struct {
	Message        string `json:"response_description"`
	HTTPStatusCode int    `json:"http_status_code"`
	Data           T      `json:"response_data"`
}

func (a *ViewEntity[T]) ToJSON() []byte {
	j, err := json.Marshal(a)
	errors.HandlerReturnedVoid(err)
	return j
}

type ViewEntityBuilder[T any] struct {
	message        string
	hTTPStatusCode int
	data           T
}

func NewViewEntityBuilder[T any]() *ViewEntityBuilder[T] {
	a := new(ViewEntityBuilder[T])
	return a
}

func (a *ViewEntityBuilder[T]) SetData(data T) *ViewEntityBuilder[T] {
	a.data = data
	return a
}

func (a *ViewEntityBuilder[T]) SetMessage(message string) *ViewEntityBuilder[T] {
	a.message = message
	return a
}

func (a *ViewEntityBuilder[T]) SetHTTPStatusCode(hTTPStatusCode int) *ViewEntityBuilder[T] {
	a.hTTPStatusCode = hTTPStatusCode
	return a
}

func (a *ViewEntityBuilder[T]) Build() *ViewEntity[T] {
	b := new(ViewEntity[T])
	b.Data = a.data
	b.HTTPStatusCode = a.hTTPStatusCode
	b.Message = a.message
	return b
}
