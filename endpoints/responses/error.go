package responses

type Error struct {
	ResponseData   any    `json:"response_data"`
	ResponseDesc   string `json:"response_description"`
	HTTPStatusCode int    `json:"http_status_code"`
}
