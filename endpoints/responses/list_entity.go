package responses

import (
	"axiata-test/pkg/errors"
	"encoding/json"
)

type ListEntity[T any] struct {
	Message        string `json:"response_description"`
	HTTPStatusCode int    `json:"http_status_code"`
	Data           []T    `json:"response_data"`
}

func (a *ListEntity[T]) ToJSON() []byte {
	j, err := json.Marshal(a)
	errors.HandlerReturnedVoid(err)
	return j
}

type ListEntityBuilder[T any] struct {
	message        string
	hTTPStatusCode int
	data           []T
}

func NewListEntityBuilder[T any]() *ListEntityBuilder[T] {
	a := new(ListEntityBuilder[T])
	return a
}

func (a *ListEntityBuilder[T]) SetData(data []T) *ListEntityBuilder[T] {
	a.data = data
	return a
}

func (a *ListEntityBuilder[T]) SetMessage(message string) *ListEntityBuilder[T] {
	a.message = message
	return a
}

func (a *ListEntityBuilder[T]) SetHttpCode(HTTPStatusCode int) *ListEntityBuilder[T] {
	a.hTTPStatusCode = HTTPStatusCode
	return a
}

func (a *ListEntityBuilder[T]) Build() *ListEntity[T] {
	b := new(ListEntity[T])
	b.Data = a.data
	b.Message = a.message
	b.HTTPStatusCode = a.hTTPStatusCode
	return b
}
