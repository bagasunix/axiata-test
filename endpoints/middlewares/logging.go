package middlewares

import (
	"axiata-test/endpoints"
	"context"
	"time"

	"go.uber.org/zap"
)

func Logging(logger *zap.Logger) endpoints.Middleware {
	return func(e endpoints.Endpoint) endpoints.Endpoint {
		return func(ctx context.Context, request any) (response any, err error) {
			defer func(begin time.Time) {
				logger.Info("transport_error",
					zap.Error(err),
					zap.Duration("took", time.Since(begin)),
				)
			}(time.Now())
			return e(ctx, request)
		}
	}
}
