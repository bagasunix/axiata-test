build-deployment:
	go build -tags musl -v -o axiata-test .

run-deployment:
	./axiata-test

run:
	go run .