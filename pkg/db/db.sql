CREATE SCHEMA IF NOT EXISTS public;

CREATE TABLE IF NOT EXISTS public.post (
  id UUID PRIMARY KEY,
  title VARCHAR,
  content VARCHAR,
  status VARCHAR,
  publish_date TIMESTAMP,
  created_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS public.tag (
  id UUID PRIMARY KEY,
  label VARCHAR,
  created_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS public.post_tag (
  post_id UUID,
  tag_id UUID,
  created_at TIMESTAMP,
  PRIMARY KEY (post_id, tag_id),
  FOREIGN KEY (post_id) REFERENCES public.post (id),
  FOREIGN KEY (tag_id) REFERENCES public.tag (id)
);
