package db

import (
	"database/sql"

	"go.uber.org/zap"
)

type SqlHandler interface {
	Exec(string, ...any) (Result, error)
	Query(string, ...any) (Row, error)
	Transaction(func() (any, error)) (any, error)
	MultiExec(string) error
	Close() error
}

type Result interface {
	LastInsertId() (int64, error)
	RowsAffected() (int64, error)
}

type Row interface {
	Scan(...any) error
	Next() bool
	Close() error
}

// sqlHandler has a connection with DB
type sqlHandler struct {
	log     *zap.Logger
	DB      *sql.DB
	connect string
}

// Exec executes the SQL that manipulates the data of the table
func (handler *sqlHandler) Exec(statement string, args ...interface{}) (Result, error) {
	handler.log.Debug("Prepare SQL statement for execution")
	stmt, err := handler.DB.Prepare(statement)
	if err != nil {
		handler.log.Error(err.Error())
		return nil, err
	}
	defer stmt.Close()

	handler.log.Debug("Execute prepared SQL statement")
	res, err := stmt.Exec(args...)
	if err != nil {
		handler.log.Error(err.Error())
		return nil, err
	}

	return &SqlResult{Result: res}, nil
}

// Query gets data from the database
func (handler *sqlHandler) Query(statement string, args ...interface{}) (Row, error) {
	handler.log.Debug("Prepare SQL statement for query")
	stmt, err := handler.DB.Prepare(statement)
	if err != nil {
		handler.log.Error(err.Error())
		return nil, err
	}
	defer stmt.Close()

	handler.log.Debug("Query prepared SQL statement")
	rows, err := stmt.Query(args...)
	if err != nil {
		handler.log.Error(err.Error())
		return nil, err
	}

	return &SqlRow{Rows: rows}, nil
}

// Transaction ...
func (handler *sqlHandler) Transaction(f func() (interface{}, error)) (interface{}, error) {
	handler.log.Debug("Begin SQL transaction")
	tx, err := handler.DB.Begin()
	if err != nil {
		handler.log.Error(err.Error())
		return nil, err
	}

	v, err := f()
	if err != nil {
		handler.log.Error(err.Error())
		handler.log.Warn("Rollback transaction")
		eRollback := tx.Rollback()
		if eRollback != nil {
			err = eRollback
		}
		return nil, err
	}

	err = tx.Commit()
	if err != nil {
		handler.log.Error(err.Error())
		handler.log.Warn("Rollback transaction")
		tx.Rollback()
		return nil, err
	}

	return v, nil
}

func (handler *sqlHandler) Close() error {
	if handler.DB != nil {
		handler.DB.Close()
	}
	return nil
}

// SqlResult ...
type SqlResult struct {
	Result sql.Result
}

// LastInsertId ...
func (r *SqlResult) LastInsertId() (int64, error) {
	res, err := r.Result.LastInsertId()
	if err != nil {
		return res, err
	}
	return res, nil
}

// RowsAffected ...
func (r *SqlResult) RowsAffected() (int64, error) {
	res, err := r.Result.LastInsertId()
	if err != nil {
		return res, err
	}
	return res, nil
}

// SqlRow ...
type SqlRow struct {
	Rows *sql.Rows
}

// Scan ...
func (r *SqlRow) Scan(dest ...interface{}) error {
	if err := r.Rows.Scan(dest...); err != nil {
		return err
	}
	return nil
}

// Next ...
func (r *SqlRow) Next() bool {
	return r.Rows.Next()
}

// Close ...
func (r *SqlRow) Close() error {
	if err := r.Rows.Close(); err != nil {
		return err
	}
	return nil
}
