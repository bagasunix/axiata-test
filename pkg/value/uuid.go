package value

import (
	"axiata-test/pkg/errors"

	"github.com/gofrs/uuid"
	"go.uber.org/zap"
)

func GenerateUUIDV4(logger *zap.Logger) uuid.UUID {
	id, err := uuid.NewV4()
	errors.HandlerWithLoggerReturnedVoid(logger, err, "uuid", "generator")
	return id
}
