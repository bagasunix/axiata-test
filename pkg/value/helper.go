package value

// Fungsi untuk memeriksa apakah sebuah tag ada dalam slice tag
func ContainsTag(tags []string, tag string) bool {
	for _, t := range tags {
		if t == tag {
			return true
		}
	}
	return false
}
