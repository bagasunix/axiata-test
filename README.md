# Axiata Test

This repository is for testing purpose

## Deployment

To deploy this repo we can refer to make file.

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/bagasunix/axiata-test
```

Go to the project directory

```bash
  cd axiata-test
```

Run docker compose for database

```bash
  docker compose up -d
```

Run main file

```bash
  go run main.go
```