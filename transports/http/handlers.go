package http

import (
	"axiata-test/endpoints"
	"axiata-test/transports/http/handlers"
	"net/http"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

func NewHttpHandler(logger *zap.Logger, endpoints endpoints.Endpoints) http.Handler {

	r := mux.NewRouter()
	handlers.MakePostHandler(logger, endpoints.PostEndpoint, "/api/posts", r)

	// Menerapkan middleware ke handler NotFound
	notFoundHandler := handlers.ApplyMiddleware(http.HandlerFunc(handlers.HandleNotFound), handlers.HandleErrors)

	// Menambahkan handler NotFound ke router
	r.NotFoundHandler = notFoundHandler

	return r
}
