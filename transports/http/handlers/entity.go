package handlers

import (
	"axiata-test/endpoints/requests"
	"axiata-test/pkg/errors"
	"context"
	"net/http"

	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
)

func decodeByEntityIdEndpoint(ctx context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, errors.ErrInvalidAttributes("id")
	}
	uuidId, err := uuid.FromString(id)
	requestBuilder := requests.NewEntityIdBuilder()
	requestBuilder.SetId(uuidId)
	return requestBuilder.Build(), err
}
