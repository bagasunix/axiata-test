package handlers

import (
	"axiata-test/endpoints"
	"axiata-test/endpoints/requests"
	"encoding/json"
	"net/http"

	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

func MakePostHandler(logger *zap.Logger, endpoints endpoints.PostEndpoint, prefixPath string, r *mux.Router) *mux.Router {
	r.PathPrefix(prefixPath).Path("").Handler(makeCreatePost(endpoints)).Methods(http.MethodPost)
	r.PathPrefix(prefixPath).Path("").Handler(makeListPost(endpoints)).Methods(http.MethodGet)
	r.PathPrefix(prefixPath).Path("/{id}").Handler(makeDeletePost(endpoints)).Methods(http.MethodDelete)
	r.PathPrefix(prefixPath).Path("/{id}").Handler(makeViewPost(endpoints)).Methods(http.MethodGet)
	r.PathPrefix(prefixPath).Path("/{id}").Handler(makeUpdatePost(endpoints)).Methods(http.MethodPut)
	return r
}

func makeCreatePost(endpoints endpoints.PostEndpoint) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req requests.CreatePost
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			mapErr := map[string]any{
				"response_description": "Gagal parse data",
				"response_data":        err.Error(),
				"http_status_code":     400,
			}
			EncodeError(r.Context(), mapErr, w)
			return
		}

		resp, err := endpoints.CreatePostEndpoint(r.Context(), &req)
		if err != nil {
			EncodeError(r.Context(), resp, w)
			return
		}

		EncodeCreate(r.Context(), w, resp)
	})
}

func makeListPost(endpoints endpoints.PostEndpoint) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		resp, err := endpoints.ListPostEndpoint(r.Context(), nil)
		if err != nil {
			EncodeError(r.Context(), resp, w)
			return
		}

		EncodeOk(r.Context(), w, resp)
	})
}

func makeDeletePost(endpoints endpoints.PostEndpoint) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id, err := decodeByEntityIdEndpoint(r.Context(), r)
		if err != nil {
			mapErr := map[string]any{
				"response_description": "ID Tidak Valid",
				"response_data":        err.Error(),
				"http_status_code":     400,
			}
			EncodeError(r.Context(), mapErr, w)
			return
		}
		resp, err := endpoints.DeletePostEndpoint(r.Context(), id)
		if err != nil {
			EncodeError(r.Context(), resp, w)
		}
		EncodeOk(r.Context(), w, resp)
	})

}
func makeViewPost(endpoints endpoints.PostEndpoint) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id, err := decodeByEntityIdEndpoint(r.Context(), r)
		if err != nil {
			mapErr := map[string]any{
				"response_description": "ID Tidak Valid",
				"response_data":        err.Error(),
				"http_status_code":     400,
			}
			EncodeError(r.Context(), mapErr, w)
			return
		}
		resp, err := endpoints.ViewPostEndpoint(r.Context(), id)
		if err != nil {
			EncodeError(r.Context(), resp, w)
		}
		EncodeOk(r.Context(), w, resp)
	})
}
func makeUpdatePost(endpoints endpoints.PostEndpoint) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id, err := decodeByEntityIdEndpoint(r.Context(), r)
		if err != nil {
			mapErr := map[string]any{
				"response_description": "ID Tidak Valid",
				"response_data":        err.Error(),
				"http_status_code":     400,
			}
			EncodeError(r.Context(), mapErr, w)
			return
		}
		idRequest, _ := id.(*requests.EntityId)
		var req requests.UpdatePost
		req.ID = idRequest.Id.(uuid.UUID)
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			mapErr := map[string]any{
				"response_description": "Gagal parse data",
				"response_data":        err.Error(),
				"http_status_code":     400,
			}
			EncodeError(r.Context(), mapErr, w)
			return
		}
		resp, err := endpoints.UpdatePostEndpoint(r.Context(), &req)
		if err != nil {
			EncodeError(r.Context(), resp, w)
			return
		}
		EncodeOk(r.Context(), w, resp)
	})
}
