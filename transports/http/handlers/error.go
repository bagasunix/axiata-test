package handlers

import (
	"axiata-test/endpoints/responses"
	"context"
	"encoding/json"
	"net/http"
)

func EncodeError(_ context.Context, resp any, w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	resps, _ := json.Marshal(resp)
	responseBuilder := responses.Error{}
	json.Unmarshal(resps, &responseBuilder)

	w.WriteHeader(responseBuilder.HTTPStatusCode)
	return json.NewEncoder(w).Encode(&responseBuilder)
}

type BaseError struct {
	ResponseData   interface{} `json:"response_data"`
	ResponseDesc   string      `json:"response_description"`
	HTTPStatusCode int         `json:"http_status_code"`
}
type Middleware func(http.Handler) http.Handler

func HandleErrors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				// Jika ada panic (Internal Server Error), berikan tanggapan 500
				resp := BaseError{
					ResponseData:   r,
					ResponseDesc:   "Internal Server Error",
					HTTPStatusCode: http.StatusInternalServerError,
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(resp.HTTPStatusCode)
				json.NewEncoder(w).Encode(resp)
			}
		}()

		// Lanjutkan ke handler berikutnya
		next.ServeHTTP(w, r)
	})
}

func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	resp := BaseError{
		ResponseData:   nil,
		ResponseDesc:   "Alamat tidak ditemukan",
		HTTPStatusCode: http.StatusNotFound,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(resp.HTTPStatusCode)
	json.NewEncoder(w).Encode(resp)
}
func ApplyMiddleware(handler http.Handler, middleware ...Middleware) http.Handler {
	for _, m := range middleware {
		handler = m(handler)
	}
	return handler
}
