package posttag

import (
	"axiata-test/domains/data/models"
	"axiata-test/pkg/errors"
	"context"
	"database/sql"

	"github.com/gofrs/uuid"
	"go.uber.org/zap"
)

type repoProvider struct {
	log *zap.Logger
	db  *sql.DB
}

// DeleteTagTx implements Repository.
func (r *repoProvider) DeleteTagTx(ctx context.Context, tx any, id uuid.UUID) error {
	_, err := tx.(*sql.Tx).ExecContext(ctx, "DELETE FROM post_tag WHERE tag_id = $1", id)
	return errors.ErrSomethingWrong(r.log, err)
}

// GetTagsByPost implements Repository.
func (r *repoProvider) GetTagsByPost(ctx context.Context, postID uuid.UUID) (result models.SliceResult[*models.Tag]) {
	row, err := r.db.QueryContext(ctx, sql_GetTagsbyPost, postID)
	if err != nil {
		result.Error = err
		return result
	}
	defer row.Close()
	var tags []*models.Tag
	for row.Next() {
		out := &models.Tag{}
		err = row.Scan(&out.ID, &out.Label)
		if err != nil {
			result.Error = err
			return result
		}
		tags = append(tags, out)
	}
	result.Error = err
	result.Value = tags
	return result
}

// GetByPostAndTag implements Repository.
func (r *repoProvider) GetByPostAndTag(ctx context.Context, postID, tagID uuid.UUID) (result models.SliceResult[*models.PostTag]) {
	row, err := r.db.QueryContext(ctx, sql_GetByPostAndTag, postID, tagID)
	if err != nil {
		result.Error = errors.ErrRecordNotFound(r.log, "title", err)
		return result
	}
	defer row.Close()
	var tags []*models.PostTag
	for row.Next() {
		out := &models.PostTag{}
		err = row.Scan(&out.PostID, &out.TagID)
		if err != nil {
			result.Error = errors.ErrRecordNotFound(r.log, "title", err)
			return result
		}
		tags = append(tags, out)
	}
	result.Error = errors.ErrRecordNotFound(r.log, "title", err)
	result.Value = tags
	return result
}

// GetByPostId implements Repository.
func (r *repoProvider) GetByPostId(ctx context.Context, id uuid.UUID) (result models.SingleResult[*models.PostTag]) {
	panic("unimplemented")
}

// GetByTagId implements Repository.
func (r *repoProvider) GetByTagId(ctx context.Context, id uuid.UUID) (result models.SingleResult[*models.PostTag]) {
	panic("unimplemented")
}

// CreateTx implements Repository.
func (r *repoProvider) CreateTx(ctx context.Context, tx any, m *models.PostTag) error {
	_, err := tx.(*sql.Tx).ExecContext(ctx, sql_InsertPostTag, m.PostID, m.TagID, m.CreatedAt)
	return errors.ErrDuplicateValue(r.log, r.GetModelName(), err)
}

// DeleteTx implements Repository.
func (r *repoProvider) DeleteTx(ctx context.Context, tx any, id uuid.UUID) error {
	_, err := tx.(*sql.Tx).ExecContext(ctx, "DELETE FROM post_tag WHERE post_id = $1", id)
	return errors.ErrSomethingWrong(r.log, err)
}

// GetConnection implements Repository.
func (r *repoProvider) GetConnection() (T any) {
	return r.db
}

// GetModelName implements Repository.
func (r *repoProvider) GetModelName() string {
	return "posttag"
}

// UpdateTx implements Repository.
func (r *repoProvider) UpdateTx(ctx context.Context, tx any, m *models.PostTag) error {
	panic("unimplemented")
}

func NewGorm(log *zap.Logger, db *sql.DB) Repository {
	g := new(repoProvider)
	g.db = db
	g.log = log
	return g
}
