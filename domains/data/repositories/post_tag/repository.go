package posttag

import (
	"axiata-test/domains/data/models"
	"axiata-test/domains/data/repositories/base"
	"context"

	"github.com/gofrs/uuid"
)

type Command interface {
	CreateTx(ctx context.Context, tx any, m *models.PostTag) error
	UpdateTx(ctx context.Context, tx any, m *models.PostTag) error
	DeleteTx(ctx context.Context, tx any, id uuid.UUID) error
	DeleteTagTx(ctx context.Context, tx any, id uuid.UUID) error
}

type Query interface {
	GetByPostId(ctx context.Context, id uuid.UUID) (result models.SingleResult[*models.PostTag])
	GetByTagId(ctx context.Context, id uuid.UUID) (result models.SingleResult[*models.PostTag])
	GetByPostAndTag(ctx context.Context, postID, tagID uuid.UUID) (result models.SliceResult[*models.PostTag])
	GetTagsByPost(ctx context.Context, postID uuid.UUID) (result models.SliceResult[*models.Tag])
}

type Repository interface {
	base.Repository
	Command
	Query
}
