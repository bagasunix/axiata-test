package posttag

const (
	sql_InsertPostTag   = `INSERT INTO post_tag (post_id, tag_id, created_at) VALUES ($1,$2,$3)`
	sql_GetByPostID     = `SELECT post_id, tag_id FROM post_tag WHERE post_id = $1 LIMIT 1`
	sql_GetByPostAndTag = `SELECT post_id, tag_id FROM post_tag WHERE post_id = $1 AND tag_id = $2`
	sql_GetTagsbyPost   = `SELECT t.id, t.label FROM tag AS t JOIN post_tag AS pt ON t.id = pt.tag_id
	WHERE pt.post_id = $1`
)
