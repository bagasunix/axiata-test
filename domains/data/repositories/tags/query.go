package tags

const (
	sql_InsertTag  = `INSERT INTO tag (id, label, created_at) VALUES ($1,$2,$3)`
	sql_GetByTitle = `SELECT id, label FROM tag WHERE LOWER(label) = $1;`
)
