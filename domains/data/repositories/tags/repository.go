package tags

import (
	"axiata-test/domains/data/models"
	"axiata-test/domains/data/repositories/base"
	"context"

	"github.com/gofrs/uuid"
)

type Command interface {
	Create(ctx context.Context, m *models.Tag) error
	CreateTx(ctx context.Context, tx any, m *models.Tag) error
	UpdateTx(ctx context.Context, tx any, m *models.Tag) error
	Delete(ctx context.Context, id uuid.UUID) error
}

type Query interface {
	GetById(ctx context.Context, id uuid.UUID) (result models.SingleResult[*models.Tag])
	GetAll(ctx context.Context) (result models.SliceResult[models.Tag])
	GetByTitle(ctx context.Context, title string) (result models.SliceResult[models.Tag])
}

type Repository interface {
	base.Repository
	Command
	Query
}
