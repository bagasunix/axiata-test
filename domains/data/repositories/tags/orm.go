package tags

import (
	"axiata-test/domains/data/models"
	"axiata-test/pkg/errors"
	"context"
	"database/sql"

	"github.com/gofrs/uuid"
	"go.uber.org/zap"
)

type repoProvider struct {
	log *zap.Logger
	db  *sql.DB
}

// Create implements Repository.
func (r *repoProvider) Create(ctx context.Context, m *models.Tag) error {
	_, err := r.db.ExecContext(ctx, sql_InsertTag, m.ID, m.Label)
	return errors.ErrDuplicateValue(r.log, r.GetModelName(), err)
}

// CreateTx implements Repository.
func (r *repoProvider) CreateTx(ctx context.Context, tx any, m *models.Tag) error {
	_, err := tx.(*sql.Tx).ExecContext(ctx, sql_InsertTag, m.ID, m.Label, m.CreatedAt)
	return errors.ErrDuplicateValue(r.log, r.GetModelName(), err)
}

// Delete implements Repository.
func (r *repoProvider) Delete(ctx context.Context, id uuid.UUID) error {
	panic("unimplemented")
}

// GetAll implements Repository.
func (r *repoProvider) GetAll(ctx context.Context) (result models.SliceResult[models.Tag]) {
	panic("unimplemented")
}

// GetById implements Repository.
func (r *repoProvider) GetById(ctx context.Context, id uuid.UUID) (result models.SingleResult[*models.Tag]) {
	panic("unimplemented")
}

// GetByTitle implements Repository.
func (r *repoProvider) GetByTitle(ctx context.Context, title string) (result models.SliceResult[models.Tag]) {
	row, err := r.db.QueryContext(ctx, sql_GetByTitle, title)
	if err != nil {
		result.Error = errors.ErrRecordNotFound(r.log, "title", err)
		return result
	}
	defer row.Close()
	var tags []models.Tag
	for row.Next() {
		out := &models.Tag{}
		dataRows := []interface{}{
			&out.ID,
			&out.Label,
		}
		err = row.Scan(dataRows...)
		if err != nil {
			result.Error = errors.ErrRecordNotFound(r.log, "title", err)
			return result
		}
		tags = append(tags, *out)
	}
	result.Error = errors.ErrRecordNotFound(r.log, "title", err)
	result.Value = tags
	return result
}

// GetConnection implements Repository.
func (r *repoProvider) GetConnection() (T any) {
	return r.db
}

// GetModelName implements Repository.
func (r *repoProvider) GetModelName() string {
	return "tag"
}

// UpdateTx implements Repository.
func (r *repoProvider) UpdateTx(ctx context.Context, tx any, m *models.Tag) error {
	panic("unimplemented")
}

func NewGorm(log *zap.Logger, db *sql.DB) Repository {
	g := new(repoProvider)
	g.db = db
	g.log = log
	return g
}
