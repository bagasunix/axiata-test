package posts

const (
	sql_InsertPost = `INSERT INTO post (id, title, content, status, publish_date, created_at) VALUES ($1, $2, $3, $4, $5, $6)`
	sql_GetByTitle = `SELECT id, title, content, status, publish_date FROM post WHERE LOWER(title) = $1;`
	sql_GetAll     = `SELECT p.id, p.title, p.content, p.status, p.publish_date, array_agg(t.id) AS tag_ids, array_agg(t.label) AS tag_labels FROM post p LEFT JOIN post_tag pt ON p.id = pt.post_id LEFT JOIN tag t ON pt.tag_id = t.id GROUP BY p.id;`
	sql_GetByID    = `SELECT p.id, p.title, p.content, p.status, p.publish_date, array_agg(t.id) AS tag_ids, array_agg(t.label) AS tag_labels FROM post p LEFT JOIN post_tag pt ON p.id = pt.post_id LEFT JOIN tag t ON pt.tag_id = t.id WHERE p.id = $1 GROUP BY p.id;`
)
