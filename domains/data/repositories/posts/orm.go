package posts

import (
	"axiata-test/domains/data/models"
	"axiata-test/pkg/errors"
	"context"
	"database/sql"
	"strings"

	"github.com/gofrs/uuid"
	"github.com/lib/pq"
	"go.uber.org/zap"
)

type repoProvider struct {
	log *zap.Logger
	db  *sql.DB
}

// GetConnection implements Repository.
func (r *repoProvider) GetConnection() (T any) {
	return r.db
}

// GetModelName implements Repository.
func (r *repoProvider) GetModelName() string {
	return "post"
}

// GetByTitle implements Repository.
func (r *repoProvider) GetByTitle(ctx context.Context, title string) (result models.SliceResult[models.Post]) {
	row, err := r.db.QueryContext(ctx, sql_GetByTitle, strings.ToLower(title))
	if err != nil {
		result.Error = errors.ErrRecordNotFound(r.log, "title", err)
		return result
	}
	defer row.Close()
	var posts []models.Post
	for row.Next() {
		out := &models.Post{}
		dataRows := []interface{}{
			&out.ID,
			&out.Title,
			&out.Content,
			&out.Status,
			&out.PublishDate,
		}

		err = row.Scan(dataRows...)
		if err != nil {
			result.Error = errors.ErrRecordNotFound(r.log, "title", err)
			return result
		}
		posts = append(posts, *out)
	}
	result.Error = errors.ErrRecordNotFound(r.log, "title", err)
	result.Value = posts
	return result
}

// Create implements Repository.
func (r *repoProvider) Create(ctx context.Context, m *models.Post) error {
	_, err := r.db.ExecContext(ctx, sql_InsertPost, m.ID, m.Title, m.Content, m.Status, m.PublishDate)
	return errors.ErrDuplicateValue(r.log, r.GetModelName(), err)
}

// CreateTx implements Repository.
func (r *repoProvider) CreateTx(ctx context.Context, tx any, m *models.Post) error {
	_, err := tx.(*sql.Tx).ExecContext(ctx, sql_InsertPost, m.ID, m.Title, m.Content, m.Status, m.PublishDate, m.CreatedAt)
	return errors.ErrDuplicateValue(r.log, r.GetModelName(), err)
}

// Delete implements Repository.
func (r *repoProvider) DeleteTx(ctx context.Context, tx any, id uuid.UUID) error {
	// Delete the post
	_, err := tx.(*sql.Tx).ExecContext(ctx, "DELETE FROM post WHERE id = $1", id)
	return errors.ErrSomethingWrong(r.log, err)
}

// GetAll implements Repository.
func (r *repoProvider) GetAll(ctx context.Context) (result models.SliceResult[models.Post]) {
	rows, err := r.db.QueryContext(ctx, sql_GetAll)
	if err != nil {
		result.Error = err
		return result
	}
	defer rows.Close()
	var posts []models.Post
	for rows.Next() {
		var post models.Post
		var tagIDs []sql.NullString
		var tagLabels []sql.NullString
		err := rows.Scan(&post.ID, &post.Title, &post.Content, &post.Status, &post.PublishDate, pq.Array(&tagIDs), pq.Array(&tagLabels))
		if err != nil {
			result.Error = err
			return
		}
		if tagIDs[0].String != "" {
			for i := range tagIDs {
				var tagID uuid.UUID
				if tagIDs[i].Valid {
					id, parseErr := uuid.FromString(tagIDs[i].String)
					if parseErr != nil {
						result.Error = parseErr
						return
					}
					tagID = id
				}
				var tagLabel string
				if tagLabels[i].Valid {
					tagLabel = tagLabels[i].String
				}
				tag := &models.Tag{
					ID:    tagID,
					Label: tagLabel,
				}
				post.Tags = append(post.Tags, tag)
			}
		}
		posts = append(posts, post)
	}
	if err := rows.Err(); err != nil {
		result.Error = err
	}

	result.Error = err
	result.Value = posts
	return result
}

// GetById implements Repository.
func (r *repoProvider) GetById(ctx context.Context, id uuid.UUID) (result models.SingleResult[*models.Post]) {
	rows, err := r.db.QueryContext(ctx, sql_GetByID, id)
	if err != nil {
		result.Error = err
		return result
	}
	defer rows.Close()
	var posts *models.Post
	for rows.Next() {
		var post models.Post
		var tagIDs []sql.NullString
		var tagLabels []sql.NullString
		err := rows.Scan(&post.ID, &post.Title, &post.Content, &post.Status, &post.PublishDate, pq.Array(&tagIDs), pq.Array(&tagLabels))
		if err != nil {
			result.Error = err
			return
		}
		if tagIDs[0].String != "" {
			for i := range tagIDs {
				var tagID uuid.UUID
				if tagIDs[i].Valid {
					id, parseErr := uuid.FromString(tagIDs[i].String)
					if parseErr != nil {
						result.Error = parseErr
						return
					}
					tagID = id
				}
				var tagLabel string
				if tagLabels[i].Valid {
					tagLabel = tagLabels[i].String
				}
				tag := &models.Tag{
					ID:    tagID,
					Label: tagLabel,
				}
				post.Tags = append(post.Tags, tag)
			}
		}
		posts = &post
	}
	if err := rows.Err(); err != nil {
		result.Error = err
	}

	result.Error = err
	result.Value = posts
	return result
}

// UpdateTx implements Repository.
func (r *repoProvider) UpdateTx(ctx context.Context, tx any, m *models.Post) error {
	_, err := tx.(*sql.Tx).ExecContext(ctx, `UPDATE post SET title = $1, content = $2, status = $3, publish_date = $4 WHERE id = $5;`, m.Title, m.Content, m.Status, m.PublishDate, m.ID)
	return errors.ErrSomethingWrong(r.log, err)
}

func NewGorm(log *zap.Logger, db *sql.DB) Repository {
	g := new(repoProvider)
	g.db = db
	g.log = log
	return g
}
