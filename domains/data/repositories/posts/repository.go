package posts

import (
	"axiata-test/domains/data/models"
	"axiata-test/domains/data/repositories/base"
	"context"

	"github.com/gofrs/uuid"
)

type Command interface {
	Create(ctx context.Context, m *models.Post) error
	CreateTx(ctx context.Context, tx any, m *models.Post) error
	UpdateTx(ctx context.Context, tx any, m *models.Post) error
	DeleteTx(ctx context.Context, tx any, id uuid.UUID) error
}

type Query interface {
	GetById(ctx context.Context, id uuid.UUID) (result models.SingleResult[*models.Post])
	GetAll(ctx context.Context) (result models.SliceResult[models.Post])
	GetByTitle(ctx context.Context, title string) (result models.SliceResult[models.Post])
}

type Repository interface {
	base.Repository
	Command
	Query
}
