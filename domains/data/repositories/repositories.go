package repositories

import (
	posttag "axiata-test/domains/data/repositories/post_tag"
	"axiata-test/domains/data/repositories/posts"
	"axiata-test/domains/data/repositories/tags"
	"database/sql"

	"go.uber.org/zap"
)

type Repositories interface {
	GetPost() posts.Repository
	GetTag() tags.Repository
	GetPostTag() posttag.Repository
}

type repo struct {
	post    posts.Repository
	tag     tags.Repository
	posttag posttag.Repository
	logger  *zap.Logger
}

// GetPostTag implements Repositories.
func (r *repo) GetPostTag() posttag.Repository {
	return r.posttag
}

// GetTag implements Repositories.
func (r *repo) GetTag() tags.Repository {
	return r.tag
}

// GetMovie implements Repositories.
func (r *repo) GetPost() posts.Repository {
	return r.post
}

func New(logger *zap.Logger, db *sql.DB) Repositories {
	rp := new(repo)
	rp.post = posts.NewGorm(logger, db)
	rp.tag = tags.NewGorm(logger, db)
	rp.posttag = posttag.NewGorm(logger, db)
	rp.logger = logger
	return rp
}
