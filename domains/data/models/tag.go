package models

import (
	"time"

	"github.com/gofrs/uuid"
)

type Tag struct {
	ID        uuid.UUID
	Label     string
	Posts     []*Post
	CreatedAt time.Time
}
