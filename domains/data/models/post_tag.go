package models

import (
	"time"

	"github.com/gofrs/uuid"
)

type PostTag struct {
	PostID    uuid.UUID
	TagID     uuid.UUID
	CreatedAt time.Time
}
