package models

import (
	"time"

	"github.com/gofrs/uuid"
)

type Post struct {
	ID          uuid.UUID
	Title       string
	Content     string
	Status      int
	PublishDate time.Time
	Tags        []*Tag
	CreatedAt   time.Time
}
