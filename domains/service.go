package domains

import (
	"axiata-test/domains/data/repositories"
	"axiata-test/domains/usecases"

	"go.uber.org/zap"
)

type Service interface {
	usecases.Post
}

type service struct {
	usecases.Post
}

type ServiceBuilder struct {
	logger       *zap.Logger
	repositories repositories.Repositories
	middlewares  []Middleware
}

func NewServiceBuilder(logger *zap.Logger, repositories repositories.Repositories) *ServiceBuilder {
	a := new(ServiceBuilder)
	a.logger = logger
	a.repositories = repositories
	return a
}

func buildService(logger *zap.Logger, repo repositories.Repositories) Service {
	svc := new(service)
	svc.Post = usecases.NewPost(logger, repo)
	return svc
}

// SetMiddlewares Setter method for the field middlewares of type []Middleware in the object ServiceBuilder
func (s *ServiceBuilder) SetMiddlewares(middlewares []Middleware) *ServiceBuilder {
	s.middlewares = middlewares
	return s
}

func (s *ServiceBuilder) Build() Service {
	svc := buildService(s.logger, s.repositories)
	for _, m := range s.middlewares {
		svc = m(s.repositories, svc)
	}
	return svc
}
