package domains

import "axiata-test/domains/data/repositories"

type Middleware func(repo repositories.Repositories, contract Service) Service
