package entities

import (
	"time"

	"github.com/gofrs/uuid"
)

type Post struct {
	ID          uuid.UUID `json:"id"`
	Title       string    `json:"title"`
	Content     string    `json:"content"`
	Status      string    `json:"status"`
	PublishDate time.Time `json:"publish_date"`
	Tags        []Tag     `json:"tags"`
	CreatedAt   time.Time `json:"created_at"`
}

type Tag struct {
	ID    uuid.UUID `json:"id"`
	Label string    `json:"label"`
}
