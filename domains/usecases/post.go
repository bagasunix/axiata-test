package usecases

import (
	"axiata-test/domains/data/models"
	"axiata-test/domains/data/repositories"
	"axiata-test/domains/entities"
	"axiata-test/endpoints/requests"
	"axiata-test/endpoints/responses"
	"axiata-test/pkg/errors"
	"axiata-test/pkg/value"
	"context"
	"database/sql"
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"go.uber.org/zap"
)

type Post interface {
	CreatePost(ctx context.Context, req *requests.CreatePost) (res *responses.BaseResponse, err error)
	ListPost(ctx context.Context) (res *responses.ListEntity[entities.Post], err error)
	DeletePost(ctx context.Context, req *requests.EntityId) (res *responses.BaseResponse, err error)
	ViewPost(ctx context.Context, req *requests.EntityId) (res *responses.ViewEntity[*entities.Post], err error)
	UpdatePost(ctx context.Context, req *requests.UpdatePost) (res *responses.ViewEntity[*entities.Post], err error)
}

type post struct {
	repo   repositories.Repositories
	logger *zap.Logger
}

// UpdatePost implements Post.
func (p *post) UpdatePost(ctx context.Context, req *requests.UpdatePost) (res *responses.ViewEntity[*entities.Post], err error) {
	responseBuilder := responses.NewViewEntityBuilder[*entities.Post]()
	if err = req.Validate(); err != nil {
		responseBuilder.SetHTTPStatusCode(400)
		responseBuilder.SetData(nil)
		responseBuilder.SetMessage("Gagal validasi id")
		return responseBuilder.Build(), err
	}

	CheckPostID := p.repo.GetPost().GetById(ctx, req.ID)
	if CheckPostID.Error != nil {
		responseBuilder.SetHTTPStatusCode(400)
		responseBuilder.SetData(nil)
		responseBuilder.SetMessage("Data tidak ditemukan")
		return responseBuilder.Build(), CheckPostID.Error
	}

	checkTitle := p.repo.GetPost().GetByTitle(ctx, req.Title)
	if len(checkTitle.Value) != 0 {
		responseBuilder.SetHTTPStatusCode(400)
		responseBuilder.SetData(nil)
		responseBuilder.SetMessage("Judul post sudah tersedia")
		return responseBuilder.Build(), err
	}

	mBuild := models.Post{}
	mBuild.ID = req.ID
	mBuild.Title = req.Title
	mBuild.Content = req.Content
	switch strings.ToLower(req.Status) {
	case "publish":
		mBuild.Status = 1
		mBuild.PublishDate = time.Now()
	case "draft":
		mBuild.Status = 0
		mBuild.PublishDate = req.PublishDate
	default:
		responseBuilder.SetHTTPStatusCode(400)
		responseBuilder.SetData(nil)
		responseBuilder.SetMessage("Status tidak valid")
		return responseBuilder.Build(), err
	}

	tx, err := p.repo.GetPost().GetConnection().(*sql.DB).Begin()
	if err != nil {
		tx.Rollback()
		responseBuilder.SetHTTPStatusCode(400)
		responseBuilder.SetData(nil)
		responseBuilder.SetMessage("Post gagal diubah")
		return responseBuilder.Build(), err
	}
	currentTags := p.repo.GetPostTag().GetTagsByPost(ctx, req.ID)
	// Buat map untuk menyimpan tag yang sudah ada
	currentTagsMap := make(map[string]string)
	for _, tag := range currentTags.Value {
		currentTagsMap[tag.Label] = tag.ID.String()
	}
	// Iterasi melalui setiap tag dalam permintaan
	for _, tag := range req.Tags {
		checkTag := p.repo.GetTag().GetByTitle(ctx, strings.ToLower(tag))
		if checkTag.Error != nil {
			responseBuilder.SetHTTPStatusCode(400)
			responseBuilder.SetData(nil)
			responseBuilder.SetMessage("Post gagal diubah")
			return responseBuilder.Build(), err
		}
		// Jika tag tidak ada dalam tag yang ada saat ini, tambahkan tag baru
		var mBuildPostTag models.PostTag
		if _, ok := currentTagsMap[tag]; !ok {
			if len(checkTag.Value) == 0 {
				// Buat tag baru di database
				mBuildTag := models.Tag{
					ID:        value.GenerateUUIDV4(p.logger),
					Label:     tag,
					CreatedAt: time.Now(),
				}
				if err := p.repo.GetTag().CreateTx(ctx, tx, &mBuildTag); err != nil {
					tx.Rollback()
					responseBuilder.SetHTTPStatusCode(400)
					responseBuilder.SetData(nil)
					responseBuilder.SetMessage("Post gagal diubah")
					return responseBuilder.Build(), err
				}

				// Buat relasi post_tag
				mBuildPostTag = models.PostTag{
					PostID:    CheckPostID.Value.ID,
					TagID:     mBuildTag.ID,
					CreatedAt: time.Now(),
				}
				if err := p.repo.GetPostTag().CreateTx(ctx, tx, &mBuildPostTag); err != nil {
					tx.Rollback()
					responseBuilder.SetHTTPStatusCode(400)
					responseBuilder.SetData(nil)
					responseBuilder.SetMessage("Post gagal diubah")
					return responseBuilder.Build(), err
				}
				continue
			}
			// Buat relasi post_tag
			mBuildPostTag = models.PostTag{
				PostID:    CheckPostID.Value.ID,
				TagID:     checkTag.Value[0].ID,
				CreatedAt: time.Now(),
			}
			if err := p.repo.GetPostTag().CreateTx(ctx, tx, &mBuildPostTag); err != nil {
				tx.Rollback()
				responseBuilder.SetHTTPStatusCode(400)
				responseBuilder.SetData(nil)
				responseBuilder.SetMessage("Post gagal diubah")
				return responseBuilder.Build(), err
			}
		}
	}
	// Hapus tag yang tidak ada dalam permintaan
	for _, currentTag := range currentTags.Value {
		// Jika tag saat ini tidak ada dalam permintaan, hapus dari database
		if !value.ContainsTag(req.Tags, currentTag.Label) {
			if err := p.repo.GetPostTag().DeleteTagTx(ctx, tx, currentTag.ID); err != nil {
				tx.Rollback()
				responseBuilder.SetHTTPStatusCode(400)
				responseBuilder.SetData(nil)
				responseBuilder.SetMessage("Post gagal diubah")
				return responseBuilder.Build(), err
			}
		}
	}

	if err := p.repo.GetPost().UpdateTx(ctx, tx, &mBuild); err != nil {
		tx.Rollback()
		responseBuilder.SetHTTPStatusCode(400)
		responseBuilder.SetData(nil)
		responseBuilder.SetMessage("Post gagal diubah")
		return responseBuilder.Build(), err
	}
	if err = errors.ErrSomethingWrong(p.logger, tx.Commit()); err != nil {
		tx.Rollback()
		responseBuilder.SetHTTPStatusCode(400)
		responseBuilder.SetData(nil)
		responseBuilder.SetMessage("Post gagal diubah")
		return responseBuilder.Build(), err
	}

	result := p.repo.GetPost().GetById(ctx, mBuild.ID)
	if result.Error != nil {
		responseBuilder.SetHTTPStatusCode(400)
		responseBuilder.SetData(nil)
		responseBuilder.SetMessage("Data tidak ditemukan")
		return responseBuilder.Build(), result.Error
	}

	var tags []entities.Tag
	for _, v := range result.Value.Tags {
		var tag entities.Tag
		tag.ID = v.ID
		tag.Label = v.Label
		tags = append(tags, tag)
	}

	entity := entities.Post{}
	entity.ID = result.Value.ID
	entity.Title = result.Value.Title
	entity.Content = result.Value.Content
	switch result.Value.Status {
	case 1:
		entity.Status = "Publish"
	case 2:
		entity.Status = "Draft"
	}
	entity.PublishDate = result.Value.PublishDate
	entity.Tags = tags

	responseBuilder.SetHTTPStatusCode(200)
	responseBuilder.SetMessage("Berhasil menampilkan detail post")
	responseBuilder.SetData(&entity)
	return responseBuilder.Build(), nil
}

// ViewPost implements Post.
func (p *post) ViewPost(ctx context.Context, req *requests.EntityId) (res *responses.ViewEntity[*entities.Post], err error) {
	responseBuilder := responses.NewViewEntityBuilder[*entities.Post]()
	if err = req.Validate(); err != nil {
		responseBuilder.SetHTTPStatusCode(400)
		responseBuilder.SetData(nil)
		responseBuilder.SetMessage("Gagal validasi id")
		return responseBuilder.Build(), err
	}
	result := p.repo.GetPost().GetById(ctx, req.Id.(uuid.UUID))
	if result.Error != nil {
		responseBuilder.SetHTTPStatusCode(400)
		responseBuilder.SetData(nil)
		responseBuilder.SetMessage("Data tidak ditemukan")
		return responseBuilder.Build(), result.Error
	}

	var tags []entities.Tag
	for _, v := range result.Value.Tags {
		var tag entities.Tag
		tag.ID = v.ID
		tag.Label = v.Label
		tags = append(tags, tag)
	}

	entity := entities.Post{}
	entity.ID = result.Value.ID
	entity.Title = result.Value.Title
	entity.Content = result.Value.Content
	switch result.Value.Status {
	case 1:
		entity.Status = "Publish"
	case 2:
		entity.Status = "Draft"
	}
	entity.PublishDate = result.Value.PublishDate
	entity.Tags = tags

	responseBuilder.SetHTTPStatusCode(200)
	responseBuilder.SetMessage("Berhasil menampilkan detail post")
	responseBuilder.SetData(&entity)
	return responseBuilder.Build(), nil
}

// UpdatePost implements Post.
func (p *post) DeletePost(ctx context.Context, req *requests.EntityId) (res *responses.BaseResponse, err error) {
	responseBuilder := responses.BaseResponse{}
	if err = req.Validate(); err != nil {
		responseBuilder.HTTPStatusCode = 400
		responseBuilder.ResponseData = err
		responseBuilder.ResponseDesc = "Gagal validasi id"
		return &responseBuilder, err
	}
	result := p.repo.GetPost().GetById(ctx, req.Id.(uuid.UUID))
	if result.Error != nil {
		responseBuilder.HTTPStatusCode = 400
		responseBuilder.ResponseData = err
		responseBuilder.ResponseDesc = "Data yang akan dihapus tidak ada"
		return &responseBuilder, result.Error
	}
	tx, err := p.repo.GetPost().GetConnection().(*sql.DB).Begin()
	if err != nil {
		tx.Rollback()
		responseBuilder.HTTPStatusCode = 400
		responseBuilder.ResponseData = err
		responseBuilder.ResponseDesc = "Post gagal dihapus"
		return &responseBuilder, err
	}
	if err = p.repo.GetPostTag().DeleteTx(ctx, tx, req.Id.(uuid.UUID)); err != nil {
		tx.Rollback()
		responseBuilder.HTTPStatusCode = 400
		responseBuilder.ResponseData = err
		responseBuilder.ResponseDesc = "Data gagal dihapus"
		return &responseBuilder, result.Error
	}
	if err = p.repo.GetPost().DeleteTx(ctx, tx, req.Id.(uuid.UUID)); err != nil {
		tx.Rollback()
		responseBuilder.HTTPStatusCode = 400
		responseBuilder.ResponseData = err
		responseBuilder.ResponseDesc = "Data gagal dihapus"
		return &responseBuilder, result.Error
	}

	if err = errors.ErrSomethingWrong(p.logger, tx.Commit()); err != nil {
		tx.Rollback()
		responseBuilder.HTTPStatusCode = 400
		responseBuilder.ResponseData = err
		responseBuilder.ResponseDesc = "Post gagal dibuat"
		return &responseBuilder, err
	}
	responseBuilder.ResponseDesc = "Data berhasil dihapus"
	responseBuilder.HTTPStatusCode = 200
	responseBuilder.ResponseData = nil
	return &responseBuilder, nil
}

// ListPost implements Post.
func (p *post) ListPost(ctx context.Context) (res *responses.ListEntity[entities.Post], err error) {
	responseBuilder := responses.NewListEntityBuilder[entities.Post]()
	mBuildPost := p.repo.GetPost().GetAll(ctx)
	if mBuildPost.Error != nil {
		responseBuilder.SetHttpCode(400)
		responseBuilder.SetMessage("Gagal menampilkan daftar film")
		return responseBuilder.Build(), mBuildPost.Error
	}

	var posts []entities.Post
	for _, v := range mBuildPost.Value {
		var tags []entities.Tag
		for _, i := range v.Tags {
			var tag entities.Tag
			tag.ID = i.ID
			tag.Label = i.Label
			tags = append(tags, tag)
		}
		var post entities.Post
		post.ID = v.ID
		post.Title = v.Title
		post.Content = v.Content
		switch v.Status {
		case 1:
			post.Status = "Publish"
		case 2:
			post.Status = "Draft"
		}
		post.PublishDate = v.PublishDate
		post.Tags = append(post.Tags, tags...)
		posts = append(posts, post)
	}

	responseBuilder.SetHttpCode(200)
	responseBuilder.SetMessage("Data berhasil ditampilkan")
	responseBuilder.SetData(posts)
	return responseBuilder.Build(), nil
}

// CreatePost implements Post.
func (p *post) CreatePost(ctx context.Context, req *requests.CreatePost) (res *responses.BaseResponse, err error) {
	resBuild := responses.BaseResponse{}
	if req.Validate() != nil {
		resBuild.HTTPStatusCode = 400
		resBuild.ResponseData = req.Validate().Error()
		resBuild.ResponseDesc = "validasi error"
		return &resBuild, req.Validate()
	}
	switch strings.ToLower(req.Status) {
	case "publish":
		req.Status = "1"
	case "draft":
		req.Status = "0"
	default:
		resBuild.HTTPStatusCode = 400
		resBuild.ResponseData = errors.CustomError("status posting invalid")
		resBuild.ResponseDesc = "Gagal mengisi status post"
		return &resBuild, req.Validate()
	}
	checktitle := p.repo.GetPost().GetByTitle(ctx, strings.ToLower(req.Title))
	if checktitle.Error != nil {
		resBuild.HTTPStatusCode = 400
		resBuild.ResponseData = err
		resBuild.ResponseDesc = "Gagal cek judul"
		return &resBuild, checktitle.Error
	}
	if len(checktitle.Value) != 0 {
		resBuild.HTTPStatusCode = 400
		resBuild.ResponseData = req.Title + " " + errors.ERR_ALREADY_EXISTS
		resBuild.ResponseDesc = "Judul post sudah tersedia"
		return &resBuild, req.Validate()
	}
	var mBuildPost models.Post
	if req.Status == "1" {
		mBuildPost = models.Post{
			ID:          value.GenerateUUIDV4(p.logger),
			Title:       req.Title,
			Content:     req.Content,
			Status:      1,
			PublishDate: time.Now(),
			CreatedAt:   time.Now(),
		}
	} else {
		mBuildPost = models.Post{
			ID:          value.GenerateUUIDV4(p.logger),
			Title:       req.Title,
			Content:     req.Content,
			Status:      0,
			PublishDate: req.PublishDate,
			CreatedAt:   time.Now(),
		}
	}

	tx, err := p.repo.GetPost().GetConnection().(*sql.DB).Begin()
	if err != nil {
		tx.Rollback()
		resBuild.HTTPStatusCode = 400
		resBuild.ResponseData = err
		resBuild.ResponseDesc = "Post gagal dibuat"
		return &resBuild, err
	}
	if err := p.repo.GetPost().CreateTx(ctx, tx, &mBuildPost); err != nil {
		tx.Rollback()
		resBuild.HTTPStatusCode = 400
		resBuild.ResponseData = err
		resBuild.ResponseDesc = "Post gagal dibuat"
		return &resBuild, err
	}

	for _, v := range req.Tags {
		mBuildPostTag := models.PostTag{}
		checkTag := p.repo.GetTag().GetByTitle(ctx, strings.ToLower(v))
		if checkTag.Error != nil {
			tx.Rollback()
			resBuild.HTTPStatusCode = 400
			resBuild.ResponseData = err
			resBuild.ResponseDesc = "Post gagal dibuat"
			return &resBuild, err
		}
		if len(checkTag.Value) == 0 {
			mBuildTag := models.Tag{
				ID:        value.GenerateUUIDV4(p.logger),
				Label:     v,
				CreatedAt: time.Now(),
			}
			if err := p.repo.GetTag().CreateTx(ctx, tx, &mBuildTag); err != nil {
				tx.Rollback()
				resBuild.HTTPStatusCode = 400
				resBuild.ResponseData = err
				resBuild.ResponseDesc = "Post gagal dibuat"
				return &resBuild, err
			}

			mBuildPostTag.PostID = mBuildPost.ID
			mBuildPostTag.TagID = mBuildTag.ID
			mBuildPostTag.CreatedAt = time.Now()
			if err := p.repo.GetPostTag().CreateTx(ctx, tx, &mBuildPostTag); err != nil {
				tx.Rollback()
				resBuild.HTTPStatusCode = 400
				resBuild.ResponseData = err
				resBuild.ResponseDesc = "Post gagal dibuat"
				return &resBuild, err
			}
			continue
		}

		mBuildPostTag.PostID = mBuildPost.ID
		mBuildPostTag.TagID = checkTag.Value[0].ID
		mBuildPostTag.CreatedAt = time.Now()
		if err := p.repo.GetPostTag().CreateTx(ctx, tx, &mBuildPostTag); err != nil {
			tx.Rollback()
			resBuild.HTTPStatusCode = 400
			resBuild.ResponseData = err
			resBuild.ResponseDesc = "Post gagal dibuat"
			return &resBuild, err
		}
	}

	if err = errors.ErrSomethingWrong(p.logger, tx.Commit()); err != nil {
		tx.Rollback()
		resBuild.HTTPStatusCode = 400
		resBuild.ResponseData = err
		resBuild.ResponseDesc = "Post gagal dibuat"
		return &resBuild, err
	}

	resBuild.HTTPStatusCode = 200
	resBuild.ResponseData = mBuildPost.ID
	resBuild.ResponseDesc = "Post berhasil dibuat"

	return &resBuild, nil
}

func NewPost(logger *zap.Logger, repo repositories.Repositories) Post {
	a := new(post)
	a.repo = repo
	a.logger = logger
	return a
}
