package middlewares

import (
	"axiata-test/domains"
	"axiata-test/domains/data/repositories"
	"axiata-test/domains/entities"
	"axiata-test/endpoints/requests"
	"axiata-test/endpoints/responses"
	"context"
	"time"

	"go.uber.org/zap"
)

type loggingMiddleware struct {
	logger *zap.Logger
	next   domains.Service
}

// UpdatePost implements domains.Service.
func (l *loggingMiddleware) UpdatePost(ctx context.Context, req *requests.UpdatePost) (res *responses.ViewEntity[*entities.Post], err error) {
	defer func(begin time.Time) {
		l.logger.Info("PostService", zap.String("request", string(req.ToJSON())), zap.String("response", string(res.ToJSON())), zap.Error(err), zap.Duration("took", time.Since(begin)))
	}(time.Now())
	return l.next.UpdatePost(ctx, req)
}

// ViewPost implements domains.Service.
func (l *loggingMiddleware) ViewPost(ctx context.Context, req *requests.EntityId) (res *responses.ViewEntity[*entities.Post], err error) {
	defer func(begin time.Time) {
		l.logger.Info("PostService", zap.String("request", string(req.ToJSON())), zap.String("response", string(res.ToJSON())), zap.Error(err), zap.Duration("took", time.Since(begin)))
	}(time.Now())
	return l.next.ViewPost(ctx, req)
}

// UpdatePost implements domains.Service.
func (l *loggingMiddleware) DeletePost(ctx context.Context, req *requests.EntityId) (res *responses.BaseResponse, err error) {
	defer func(begin time.Time) {
		l.logger.Info("PostService", zap.String("request", string(req.ToJSON())), zap.String("response", string(res.ToJSON())), zap.Error(err), zap.Duration("took", time.Since(begin)))
	}(time.Now())
	return l.next.DeletePost(ctx, req)
}

// ListPost implements domains.Service.
func (l *loggingMiddleware) ListPost(ctx context.Context) (res *responses.ListEntity[entities.Post], err error) {
	defer func(begin time.Time) {
		l.logger.Info("PostService", zap.String("request", ""), zap.String("response", string(res.ToJSON())), zap.Error(err), zap.Duration("took", time.Since(begin)))
	}(time.Now())
	return l.next.ListPost(ctx)
}

// CreatePost implements domains.Service.
func (l *loggingMiddleware) CreatePost(ctx context.Context, req *requests.CreatePost) (res *responses.BaseResponse, err error) {
	defer func(begin time.Time) {
		l.logger.Info("PostService", zap.String("request", string(req.ToJSON())), zap.String("response", string(res.ToJSON())), zap.Error(err), zap.Duration("took", time.Since(begin)))
	}(time.Now())
	return l.next.CreatePost(ctx, req)
}

func LoggingMiddleware(logger *zap.Logger, repo repositories.Repositories) domains.Middleware {
	return func(repo repositories.Repositories, next domains.Service) domains.Service {
		return &loggingMiddleware{logger: logger, next: next}
	}
}
