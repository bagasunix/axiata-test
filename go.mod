module axiata-test

go 1.22.3

require github.com/gofrs/uuid v4.4.0+incompatible

require (
	github.com/asaskevich/govalidator v0.0.0-20230301143203-a9d515a09cc2 // indirect
	github.com/golang-migrate/migrate v3.5.4+incompatible // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	go.uber.org/multierr v1.10.0 // indirect
)

require (
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/golang-migrate/migrate/v4 v4.17.1
	github.com/lib/pq v1.10.9 // indirect
	go.uber.org/zap v1.27.0
	gopkg.in/yaml.v3 v3.0.1
)
